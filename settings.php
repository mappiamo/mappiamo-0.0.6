<?php

// no direct access to this file
defined( 'DACCESS' ) or die;

class MSettings{
    
	    static $sitename = 'Moto Guzzi World Club';
	    static $domain = 'motoguzziworldclub.it';
	    static $location = 'Via E. Parodi 57, 23826 Mandello del Lario';

	    static $c_key = 'kHi2DH0m69UB';
	    
	    static $db = 'motoguzz_mapi';
	    static $db_type = 'mysql';
	    static $db_host = 'localhost';
	    static $db_prefix = 'mapi';
	    static $db_user = 'mgwcuser';
	    static $db_pass = 'valami';

	    static $reg_email = '';
	    static $reg_email_user = '';
	    static $reg_email_pass = '';
	    static $reg_email_host = '';
    
}

?>
